%global perl_package_name PerlIO-via-QuotedPrint

Name:           perl-PerlIO-via-QuotedPrint
Version:        0.08
Release:        397
Summary:        PerlIO layer for quoted-printable strings
License:        GPL+ or Artistic
Group:          Development/Libraries
URL:            http://search.cpan.org/dist/PerlIO-via-QuotedPrint/
Source0:        http://www.cpan.org/authors/id/S/SH/SHAY/%{perl_package_name}-%{version}.tar.gz
BuildArch:      noarch

BuildRequires:	gcc
BuildRequires:  coreutils
BuildRequires:  findutils
BuildRequires:  make
BuildRequires:  perl-interpreter
BuildRequires:  perl-generators
BuildRequires:  perl(Config)
BuildRequires:  perl(ExtUtils::MakeMaker)
BuildRequires:  perl(MIME::QuotedPrint)
BuildRequires:  perl(strict)
BuildRequires:  perl(Test::More)
BuildRequires:  perl(warnings)
Requires:       perl(:MODULE_COMPAT_%(eval "`perl -V:version`"; echo $version))
Conflicts:      perl < 4:5.22.0-347

%description
This module implements a PerlIO layer that works on files encoded in the
quoted-printable format. It will decode from quoted-printable while
reading from a handle, and it will encode as quoted-printable while
writing to a handle.

%prep
%autosetup -n %{perl_package_name}-%{version}

%package_help

%build
perl Makefile.PL INSTALLDIRS=vendor
%make_build

%install
make pure_install DESTDIR=$RPM_BUILD_ROOT
find $RPM_BUILD_ROOT -type f -name .packlist -delete
%{_fixperms} -c $RPM_BUILD_ROOT

%check
make test

%files
%doc CHANGELOG README
%{perl_vendorlib}/*

%files help
%{_mandir}/man3/*

%changelog
* Thu Sep 26 2019 shenyangyang<shenyangyang4@huawei.com> - 0.08-397
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:revise help package

* Tue Aug 13 2019 openEuler Buildteam <buildteam@openeuler.org> -0.08-396
- Package Init
